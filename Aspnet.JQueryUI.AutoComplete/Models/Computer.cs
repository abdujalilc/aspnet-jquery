﻿namespace Aspnet.JQueryUI.AutoComplete.Models
{
    public class Computer
    {
        public int Id { get; set; }
        public string? Model { get; set; }
        public int Year { get; set; }
    }
}
