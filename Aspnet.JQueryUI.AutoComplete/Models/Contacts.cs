﻿public class Contacts
{
    public string CustomerID { get; set; }
    public string CustomerName { get; set; }
    public Contacts(string CustomerName, string CustomerID)
    {
        this.CustomerID = CustomerID;
        this.CustomerName = CustomerName;
    }
}