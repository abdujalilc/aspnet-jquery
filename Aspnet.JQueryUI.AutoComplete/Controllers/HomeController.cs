﻿using Aspnet.JQueryUI.AutoComplete.Models;
using Microsoft.AspNetCore.Mvc;

namespace Aspnet.JQueryUI.AutoComplete.Controllers
{
    public class HomeController : Controller
    {
        static List<Computer> comps = new List<Computer>();

        static HomeController()
        {
            comps.Add(new Computer { Id = 1, Model = "IBM PC", Year = 1981 });
            comps.Add(new Computer { Id = 2, Model = "Apple II", Year = 1977 });
            comps.Add(new Computer { Id = 3, Model = "Apple III", Year = 1980 });
            comps.Add(new Computer { Id = 4, Model = "Macintosh", Year = 1984 });
        }
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SimpleWithList()
        {
            return View();
        }

        public ActionResult AutocompleteSearch(string term)
        {
            var models = comps.Where(a => a.Model.Contains(term))
                            .Select(a => new { value = a.Model })
                            .Distinct();

            return Json(models);
        }
        public IActionResult AutoCompleteStatic()
        {
            return View();
        }
        public IActionResult AutoCompleteAjax()
        {
            return View();
        }
        [HttpPost]
        public JsonResult AutoComplete(string prefix)
        {
            List<Contacts> customers = new List<Contacts>();

            if (!string.IsNullOrEmpty(prefix))
            {
                customers.Add(new Contacts("Tom Smith", "1"));
                customers.Add(new Contacts("Jim Carrey", "2"));
                customers.Add(new Contacts("Adam Sandler", "3"));
                customers.Add(new Contacts("Russel Craw", "4"));
                customers.Add(new Contacts("Kim Bassinger", "5"));
                var custs = customers.Where(x => x.CustomerName.Contains(prefix)).Select(x => new
                {
                    label = x.CustomerName,
                    val = x.CustomerID
                }).ToList();
                return Json(custs);
            }
            return Json(customers);
        }
        [HttpPost]
        public ActionResult AutoCompleteAjax(string CustomerName, string CustomerId)
        {
            ViewBag.Message = "CustomerName: " + CustomerName + " || CustomerId: " + CustomerId;
            return View();
        }
    }
}
