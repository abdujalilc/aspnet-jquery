﻿using Aspnet.Jquery.JqGrid.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace Aspnet.Jquery.JqGrid.Controllers
{
    public class HomeController : Controller
    {
        static List<Book> books = new List<Book>();
        static HomeController()
        {
            books.Add(new Book { Id = 1, Name = "Война и мир", Author = "Л. Толстой", Year = 1863, Price = 220 });
            books.Add(new Book { Id = 2, Name = "Отцы и дети", Author = "И. Тургенев", Year = 1862, Price = 195 });
            books.Add(new Book { Id = 3, Name = "Чайка", Author = "А. Чехов", Year = 1895, Price = 158 });
            books.Add(new Book { Id = 4, Name = "Подросток", Author = "Ф. Достоевский", Year = 1875, Price = 210 });
        }

        public ActionResult Example_1()
        {
            return View();
        }
        public ActionResult Example_2()
        {
            return View();
        }
        public ActionResult Example_3()
        {
            return View();
        }

        public string GetData()
        {
            return JsonConvert.SerializeObject(books);
        }
        [HttpPost]
        public void Edit(Book book)
        {
            // действия по редактированию
        }

        [HttpPost]
        public void Create(Book book)
        {
            // действия по добавлению
        }
        [HttpPost]
        public void Delete(int id)
        {
            // действия по удалению
        }
    }
}
