﻿using Microsoft.AspNetCore.Mvc;

namespace ExportToExcel.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
