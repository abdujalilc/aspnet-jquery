﻿using ExportToExcel.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace WebApplication1.Pages
{
    public class Example_1Model : PageModel
    {
        private DBCtx Context { get; }

        public Example_1Model(DBCtx _context)
        {
            this.Context = _context;
        }

        public List<Customer> Customers { get; set; }

        public void OnGet()
        {
            this.Customers = this.Context.Customers.ToList();
        }
    }
}