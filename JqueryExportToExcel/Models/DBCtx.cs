﻿using Microsoft.EntityFrameworkCore;

namespace ExportToExcel.Models
{
    public class DBCtx : DbContext
    {
        public DBCtx(DbContextOptions<DBCtx> options) : base(options)
        {

        }

        public DbSet<Customer> Customers { get; set; }
    }
}
