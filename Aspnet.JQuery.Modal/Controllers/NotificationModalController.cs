﻿using Microsoft.AspNetCore.Mvc;

namespace Aspnet.JQuery.Modal.Controllers
{
    public class NotificationModalController : Controller
    {
        [Route("NotificationModal/Index/{note_type?}")]
        public IActionResult Index(string? note_type)
        {
            if (note_type == "success")
                TempData["Success"] = "The result is successful";
            else if (note_type == "info")
                TempData["Info"] = "Some info from action";
            else if(note_type == "error")
                TempData["Error"] = "The result is with error";
            var test=Request.Query.ToList();
            return View();
        }
    }
}
