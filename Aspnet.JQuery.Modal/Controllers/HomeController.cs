﻿using Aspnet.JQuery.Modal.Models;
using Microsoft.AspNetCore.Mvc;

namespace Aspnet.JQuery.Modal.Controllers
{
    public class HomeController : Controller
    {
        public readonly CompContext db;
        public HomeController(CompContext _compContext)
        {
            db = _compContext;
        }
        public ActionResult Index()
        {
            return View();
        }
    }
}