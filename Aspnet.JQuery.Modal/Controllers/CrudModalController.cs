﻿using Aspnet.JQuery.Modal.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace Aspnet.JQuery.Modal.Controllers
{
    public class CrudModalController : Controller
    {
        public readonly CompContext db;
        public CrudModalController(CompContext _compContext)
        {
            db = _compContext;
        }
        public ActionResult Index()
        {
            return View(db.Computers);
        }
        // Просмотр подробных сведений о книге
        public ActionResult Details(int id)
        {
            Computer comp = db.Computers.Find(id);
            return comp != null ? PartialView("Details", comp) : View("Index");
        }
        // Добавление
        public ActionResult Create()
        {
            return PartialView("Create");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Computer comp)
        {
            _ = db.Computers.Add(comp);
            _ = db.SaveChanges();
            return RedirectToAction("Index");
        }
        // Редактирование
        public ActionResult Edit(int id)
        {
            Computer comp = db.Computers.Find(id);
            return comp != null ? PartialView("Edit", comp) : View("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(Computer comp)
        {
            db.Entry(comp).State = EntityState.Modified;
            _ = db.SaveChanges();
            return RedirectToAction("Index");
        }
        // Удаление
        public ActionResult Delete(int id)
        {
            Computer comp = db.Computers.Find(id);
            return comp != null ? PartialView("Delete", comp) : View("Index");
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        [ActionName("Delete")]
        public ActionResult DeleteRecord(int id)
        {
            Computer comp = db.Computers.Find(id);

            if (comp != null)
            {
                _ = db.Computers.Remove(comp);
                _ = db.SaveChanges();
            }
            return RedirectToAction("Index");
        }
    }
}