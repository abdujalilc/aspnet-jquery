﻿using Microsoft.EntityFrameworkCore;

namespace Aspnet.JQuery.Modal.Models
{
    public class CompContext : DbContext
    {
        public CompContext(DbContextOptions dbContextOptions) : base(dbContextOptions)
        {

        }
        public DbSet<Computer> Computers { get; set; }
    }
}
