using Aspnet.JQuery.Modal.Models;

var builder = WebApplication.CreateBuilder(args);
string? con_string = "Data Source=AppData\\QuestionnaireDB.db";
builder.Services.AddSqlite<CompContext>(con_string);
// Add services to the container.
builder.Services.AddControllersWithViews();

var app = builder.Build();



app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");

app.Run();
