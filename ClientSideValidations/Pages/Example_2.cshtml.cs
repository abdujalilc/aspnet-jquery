using ClientSideValidations.Models;
using Microsoft.AspNetCore.Mvc.RazorPages;

namespace ClientSideValidations.Pages
{
    public class Example_2Model : PageModel
    {
        public void OnGet()
        {

        }

        public void OnPost(PersonModel person)
        {
            if (ModelState.IsValid)
            {
                // Validation success.
            }
        }
    }
}
