﻿using System.ComponentModel.DataAnnotations;

namespace ClientSideValidations.Models
{
    public class PersonModel
    {
        [Display(Name = "GST Number:")]
        [Required(ErrorMessage = "Invalid GST Number.")]
        [RegularExpression(@"^[0-9]{2}[A-Z]{5}[0-9]{4}[A-Z]{1}[1-9A-Z]{1}Z[0-9A-Z]{1}$", ErrorMessage = "Invalid GST Number.")]
        public string GSTNumber { get; set; }
    }
}
