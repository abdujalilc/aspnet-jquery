﻿namespace Aspnet.Jquery.Ajax.Models
{
    public class UnregStaffVM
    {
        public string? userId { get; set; }
        public int? ModuleId { get; set; }
        public string? role { get; set; }
    }
}
