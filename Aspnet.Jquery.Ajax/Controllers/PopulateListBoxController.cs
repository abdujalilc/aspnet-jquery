﻿using Microsoft.AspNetCore.Mvc;

namespace Aspnet.Jquery.Ajax.Controllers
{
    public class PopulateListBoxController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpGet]
        public JsonResult GetCategories()
        {
            Dictionary<string, string> lstCategories = new Dictionary<string, string>();
            lstCategories.Add("1", "Beverages");
            lstCategories.Add("2", "Condiments");
            lstCategories.Add("3", "Confections");
            lstCategories.Add("4", "Dairy Products");
            lstCategories.Add("5", "Grains/Cereals");
            lstCategories.Add("6", "Meat/Poultry");
            lstCategories.Add("7", "Produce");
            lstCategories.Add("8", "Seafood");
            return Json(lstCategories);
        }
    }
}
