﻿using Microsoft.AspNetCore.Mvc;

namespace Aspnet.Jquery.Ajax.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
    }
}
