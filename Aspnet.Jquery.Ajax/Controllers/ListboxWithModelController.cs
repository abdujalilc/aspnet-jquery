﻿using Aspnet.Jquery.Ajax.Models;
using Microsoft.AspNetCore.Mvc;

namespace Aspnet.Jquery.Ajax.Controllers
{
    public class ListboxWithModelController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        public JsonResult GetCategoriesWithModel()
        {
            List<Category> lstCateogories = new List<Category>();
            lstCateogories.Add(new Category() { CategoryID = "1", CategoryName = "Beverages" });
            lstCateogories.Add(new Category() { CategoryID = "2", CategoryName = "Condiments" });
            lstCateogories.Add(new Category() { CategoryID = "3", CategoryName = "Confections" });
            lstCateogories.Add(new Category() { CategoryID = "4", CategoryName = "Dairy Products" });
            lstCateogories.Add(new Category() { CategoryID = "5", CategoryName = "Grains/Cereals" });
            lstCateogories.Add(new Category() { CategoryID = "6", CategoryName = "Meat/Poultry" });
            lstCateogories.Add(new Category() { CategoryID = "7", CategoryName = "Produce" });
            lstCateogories.Add(new Category() { CategoryID = "8", CategoryName = "Seafood" });

            return Json(lstCateogories);
        }
    }
}
