﻿using Microsoft.AspNetCore.Mvc;

namespace Aspnet.Jquery.Ajax.Area.ContentType.Controllers
{
    [Area("ContentType")]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult IndexForm(DataRequest dataRequest)
        {
            var request = Request;
            return Json(new { test = "hello" });
        }
        [HttpPost]
        public IActionResult IndexBody([FromBody] DataRequest dataRequest)
        {
            var request = Request;
            return Json(new { test = "hello" });
        }

        public record DataRequest(string userId, string ModuleId, string role);
    }
}
