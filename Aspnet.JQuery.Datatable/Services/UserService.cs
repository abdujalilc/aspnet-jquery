﻿using Aspnet.JQuery.Datatable.Models;
using System.Reflection;
using System.Text.Json;

namespace Aspnet.JQuery.Datatable.Services
{
    public interface IUserService
    {
        DataTableOutputParams<User> ListUsersForDataTable(DataTableInputParams dataTableOutputParams, string userID);
    }
    public class UserService : IUserService
    {
        private IWebHostEnvironment env;

        public UserService(IWebHostEnvironment env)
        {
            this.env = env;
        }
        public DataTableOutputParams<User> ListUsersForDataTable(DataTableInputParams inputParams, string userID)
        {
            DataTableOutputParams<User> returnResult = new DataTableOutputParams<User>();
            string search = inputParams.search,
                   sortColumn = inputParams.sortColumn,
                   sortColumnDir = inputParams.sortColumnDir;

            int skip = inputParams.skip,
                take = inputParams.take;

            IEnumerable<User>? returnedUsers = GetUsers();
            //recordsTotal
            returnResult.recordsTotal = returnedUsers.Count();

            if (!string.IsNullOrEmpty(search))
                returnedUsers = returnedUsers.Where(x => x.name.Contains(search));
            if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDir)))
            {
                if (sortColumn.ToLower() == "id")
                    returnedUsers = sortColumnDir == "asc" ? returnedUsers.OrderBy(x => x.id) : returnedUsers.OrderByDescending(x => x.id);
                else if (sortColumn.ToLower() == "name")
                    returnedUsers = sortColumnDir == "asc" ? returnedUsers.OrderBy(x => x.name) : returnedUsers.OrderByDescending(x => x.name);
                else if (sortColumn.ToLower() == "address")
                    returnedUsers = sortColumnDir == "asc" ? returnedUsers.OrderBy(x => x.address) : returnedUsers.OrderByDescending(x => x.address);

            }
            //data
            returnResult.Data = returnedUsers.Skip(skip).Take(take).ToList();
            //recordsFiltered
            returnResult.recordsFiltered = returnedUsers.Count();

            return returnResult;
        }
        private IEnumerable<User> GetUsers()
        {
            IEnumerable<User>? users = new List<User>();
            try
            {
                string json_path = env.WebRootPath + "/json/data.json";
                using (StreamReader r = new StreamReader(json_path))
                {
                    string json = r.ReadToEnd();
                    users = JsonSerializer.Deserialize<List<User>>(json);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return users;
        }
    }

}
