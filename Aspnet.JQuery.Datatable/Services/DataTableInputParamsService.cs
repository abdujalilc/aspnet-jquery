﻿using Aspnet.JQuery.Datatable.Models;

namespace Aspnet.JQuery.Datatable.Services
{
    public interface IDataTableInputParamsService
    {
        DataTableInputParams ToModel(IFormCollection form);
    }
    public class DataTableInputParamsService : IDataTableInputParamsService
    {
        public DataTableInputParams ToModel(IFormCollection form)
        {
            DataTableInputParams model = new DataTableInputParams();

            string? start = form["start"].FirstOrDefault();
            string? length = form["length"].FirstOrDefault();
            string? search = form["search[value]"].FirstOrDefault();
            string? sortColumn = form["columns[" + form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
            string? sortColumnDir = form["order[0][dir]"].FirstOrDefault();

            model.take = length != null ? Convert.ToInt32(length) : 0;
            model.skip = start != null ? Convert.ToInt32(start) : 0;
            model.search = search ?? "";
            model.sortColumn = sortColumn ?? "";
            model.sortColumnDir = sortColumnDir ?? "";

            return model;
        }
    }
}
