﻿
using Aspnet.JQuery.Datatable.Models;
using Aspnet.JQuery.Datatable.Services;
using Microsoft.AspNetCore.Mvc;

namespace Aspnet.JQuery.Datatable.Controllers
{
    public class AjaxController : Controller
    {
        private readonly IDataTableInputParamsService dataTableInputParamsService;
        private readonly IUserService userService;
        public AjaxController(IDataTableInputParamsService dataTableInputParamsService, IUserService userService)
        {
            this.dataTableInputParamsService = dataTableInputParamsService;
            this.userService = userService;
        }
        public IActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public IActionResult getUsers(IFormCollection form)
        {
            DataTableInputParams dataTableInputParams = dataTableInputParamsService.ToModel(form);
            DataTableOutputParams<User> rResult = userService.ListUsersForDataTable(dataTableInputParams, null);     

            if (rResult.Data != null && rResult.Data.Any())
                return Json(new { recordsFiltered = rResult.recordsFiltered, recordsTotal = rResult.recordsTotal, data = rResult.Data });
            else
                return Json(new EmptyResult());
        }
        
    }
}
