using Aspnet.JQuery.Datatable.Services;

var builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllersWithViews();
builder.Services.AddScoped<IUserService, UserService>();
builder.Services.AddScoped<IDataTableInputParamsService, DataTableInputParamsService>();
var app = builder.Build();

app.UseStaticFiles();
app.UseRouting();
app.MapControllerRoute(
    name: "default",
    pattern: "{controller=ajax}/{action=index}/{id?}");

app.Run();