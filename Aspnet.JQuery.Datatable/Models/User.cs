﻿namespace Aspnet.JQuery.Datatable.Models
{
    public class User
    {
        public int id { get; set; }
        public string? name { get; set; }
        public string? address { get; set; }
    }
}
