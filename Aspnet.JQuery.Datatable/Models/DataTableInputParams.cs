﻿namespace Aspnet.JQuery.Datatable.Models
{
    public class DataTableInputParams
    {
        public string search { get; set; }
        public int skip { get; set; }
        public int take { get; set; }
        public string sortColumn { get; set; }
        public string sortColumnDir { get; set; }
    }
}
