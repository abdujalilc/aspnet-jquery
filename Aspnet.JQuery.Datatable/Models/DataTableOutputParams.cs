﻿namespace Aspnet.JQuery.Datatable.Models
{
    public class DataTableOutputParams<T>
    {
        public string draw { get; set; }
        public int recordsFiltered { get; set; }
        public int recordsTotal { get; set; }
        public List<T>? Data { get; set; }
    }
}
