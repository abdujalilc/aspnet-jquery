# Aspnet.JQuery Project

This repository contains various implementations of jQuery components integrated with ASP.NET.

## Project Structure

- **Aspnet.JQuery.Datatable** - Implementation of jQuery DataTables for ASP.NET.
- **Aspnet.JQuery.Modal** - jQuery modal dialogs integrated with ASP.NET.
- **Aspnet.JQueryUI.AutoComplete** - jQuery UI Autocomplete for ASP.NET.
- **Aspnet.Jquery.Ajax** - AJAX handling with jQuery in ASP.NET.
- **Aspnet.Jquery.DateTimePicker** - jQuery DateTimePicker usage in ASP.NET.
- **Aspnet.Jquery.JqGrid** - Integration of jQuery JqGrid in ASP.NET.
- **ClientSideValidations** - Client-side form validations using jQuery.
- **JqueryExportToExcel** - jQuery-based Excel export functionality.

## Requirements

- .NET Framework / .NET Core
- jQuery
- jQuery UI (if required)
- DataTables.js (if required)

## Setup & Installation

1. Clone the repository:
   ```sh
   git clone https://gitlab.com/your-repo.git
   ```
2. Open `Aspnet.JQuery.sln` in Visual Studio.
3. Restore dependencies:
   ```sh
   nuget restore Aspnet.JQuery.sln
   ```
4. Build and run the project.

## Contribution

Feel free to contribute by submitting issues or pull requests.

## License

This project is licensed under the MIT License.
